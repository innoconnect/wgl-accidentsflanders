"use strict";
/**
 * PopupWin, a dialog component used to display details when a user clicks on a point in the map
 * require, D3v3 a Jquery
 * @param map_win_id {String} selector ('.class' or '#id'), which covers map window
 * @param idt_dim {String} IdentifyDimension
 * @param title {String} tittle for popup window
 * @param pts {Array} coords of pts [x1, y1, x2, y2, ...]
 * @param options {Object} formatting options to be passed to the IdentifyDimension when retrieving data about a point
 * @param options.delimiter {String} either a double quote """ or a single quote "'", used to correctly read fields from the CSV files containing points' data
 * @param options.separator {String} either a comma "," or a semicolon ";", used to correctly read fields from the CSV files containing points' data
 * @param geohash {WGL.ui.GeoHash} [null] - a Geohash object used to retrieve data on overlapping points on the map
 * @constructor
 */

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

WGL.ui.PopupWin = function (map_win_id, idt_dim, title, pts, options, geohash) {
    var _this2 = this;

    if (typeof pts === "undefined") {
        pts = null;
    }

    if (typeof options === "undefined") {
        options = {};
    }

    if (typeof geohash === "undefined") {
        geohash = null;
    }

    if (_typeof(WGL.ui.PopupWin.instance) === "object") {
        return WGL.ui.PopupWin.instance;
    }

    var visible = false;
    var enabled = true;
    var geohash_path = "";
    var geohash_level = 8;
    var posX = 0; // position of window
    var hideDates = false;

    var posY = 0;
    var dragged = 0;
    var threshold = 2;
    var permalink_input = null;
    var last_content = null;
    var lngLat = null;
    var overlapping_points_index = 0;
    var priority = null;
    var timeout = 0;
    this.options = options;
    var dateSelectorWidth = "6em";
    /**
     * Set visibility
     * @param {boolean} bol
     */

    var setVisibility = function setVisibility(bol) {
        d3.select("#wgl-point-win").classed("wgl-active", bol);
        d3.select("#triangle").classed("wgl-active", bol);
        visible = bol;
    };

    this.setEnabled = function (b) {
        enabled = b;
    };

    this.setGeohashPath = function (p) {
        if (typeof p === "string") {
            geohash_path = p;
        }
    };

    this.setGeohashLevel = function (l) {
        if (typeof l === "number") {
            geohash_level = l;
        }
    };

    this.setHideDates = function(b) {
        if (typeof b === "boolean") {
            hideDates = b;
        }
    };

    /**
     * Set an array of CSS selectors for dialogs with higher priority on screen.
     * If one of these is currently visible on screen, the PopupWin won't be displayed
     * @param p list of selectors
     */


    this.setPriority = function (p) {
        if (_instanceof(p, Array)) {
            priority = p;
            timeout = priority.length > 0 ? 200 : 0;
        }
    };

    this.getInstance = function () {
        if (_typeof(WGL.ui.PopupWin.instance) === "object") {
            return WGL.ui.PopupWin.instance;
        }
    };
    /**
     * Sets position of popup window
     * @param {int} x pixels
     * @param {int} y pixels
     */


    var setPosition = function setPosition(x, y) {
        if (x < 0 || y < 0) {
            setVisibility(false);
            return;
        } else {
            setVisibility(true);
        } // IN CASE OF OPENLAYERS A CORRECTION FOR THE NEW POSITION INCLUDING THE TRIANGLE HEIGHT IS NECESSARY
        // MapBox-GL supports only WGS-84, so there is no API to get or set the current projection system


        var shouldIncludeTriangleHeight = typeof map.projection !== "undefined";
        posX = x;
        posY = y;
        var mapHeight = parseInt($("#map").css("height").replace("px", ""));
        var win = $("#wgl-point-win");
        win.css("bottom", mapHeight - posY + 30 + (shouldIncludeTriangleHeight ? 35 : 0) + "px");
        win.css("left", posX - 50 + "px");
        var tri = $("#triangle");
        tri.css("top", posY - 30 - (shouldIncludeTriangleHeight ? 35 : 0) + "px");
        tri.css("left", posX - 18 + "px");
    };
    /**
     * Add html content to popup window
     * @param {string} s html
     */


    var addContent = function addContent(s) {
        $("#wgl-point-win-context").html(s);
    };

    var replaceTable = function replaceTable(s) {
        $("#details-content").html(s);
    };

    var prop2html = function prop2html(t) {
        var s = "<table>";

        for (var k in t) {
            s += "<tr><td>" + k + ": </td><td>" + t[k] + "</td></tr>";
        }

        s += "</table>";
        return s;
    };
    /**
     * @callback prop2htmlConversion
     * @param t
     */

    /**
     * Setting function for conversion from properties object to html
     * @param {prop2htmlConversion} func conversion function
     */


    this.setProp2html = function (func) {
        prop2html = func;
    }; // move map about pixel


    var movemap = function movemap(dx, dy) {};
    /**
     * Moves the map by dx, dy pixels
     * @callback moveMapCallBack
     * @param {int} dx
     * @param {int} dy
     */

    /**
     * Set function for fo moving.
     * @param {moveMapCallBack} mmf
     */


    this.setMovemap = function (mmf) {
        movemap = mmf;
    };

    this.configurePermalinkInput = function (p) {
        permalink_input = p;

        if (permalink_input != null) {
            $(map_win_id).off("popup:update-permalink").on("popup:update-permalink", function () {
                var oldURL = $("#" + permalink_input).val();

                if (visible && lngLat != null) {
                    oldURL = updateURLParameter(oldURL, encodeURIComponent(idt_dim), "");
                    oldURL = updateURLParameter(oldURL, "sc", "");
                    oldURL = updateURLParameter(oldURL, "sp", "");
                    var position = [lngLat.lng, lngLat.lat, overlapping_points_index];
                    var newURL = updateURLParameter(oldURL, encodeURIComponent(idt_dim), position.toString());

                    if (oldURL !== newURL) {
                        $("#" + permalink_input).val(newURL);
                    }
                } else {
                    var _newURL = updateURLParameter(oldURL, encodeURIComponent(idt_dim), "");

                    if (oldURL !== _newURL) {
                        $("#" + permalink_input).val(_newURL);
                    }
                }
            });
        }
    };

    this.setup = function () {
        var _this = this;

        // write elements to body
        var main = d3.select("body").insert("div").attr("id", "wgl-point-win").classed("wgl-point-selection-win", true);
        var head = main.insert("div").attr("id", "wgl-point-win-head");
        head.text(title);
        main.append("div").attr("id", "wgl-win-close").insert("i").classed("fa", true).classed("fa-times", true).attr("aria-hidden", "true");
        main.insert("div").attr("id", "wgl-point-win-context"); // event registration

        var mwid = $(map_win_id);
        mwid.off("mousedown").on("mousedown", function () {
            dragged = 0;
        });
        mwid.off("mousemove").on("mousemove", function (e) {
            var idt = WGL.getDimension(idt_dim);

            if (idt.getEnabled()) {
                dragged++; //pointer

                var num_points = idt.identify(e.offsetX, e.offsetY)[0];

                if (num_points > 0) {
                    mwid.css("cursor", "pointer");
                } else {
                    mwid.css("cursor", "default");
                }
            }
        });
        mwid.off("mouseup").on("mouseup", function (e) {
            var idt = WGL.getDimension(idt_dim);

            if (!idt.getEnabled()) {
                return;
            }

            if (!enabled) {
                return;
            }

            if (dragged < threshold) {
                var num_points = idt.identify(e.offsetX, e.offsetY)[0];

                if (num_points === 0) {
                    _this.close();

                    return;
                }

                setTimeout(function () {
                    if (priority != null) {
                        var s = 0,
                            priorityLength = priority.length;

                        while (s !== priorityLength) {
                            if ($(priority[s]).length > 0 && $(priority[s]).css("display") !== "none") {
                                _this.close();

                                return;
                            }

                            s++;
                        }
                    }

                    showDialog(e);

                    if ($(".link-permalink").length > 0) {
                        $(".link-permalink").trigger("permalink:change");
                    }
                }, timeout);
            }

            dragged = 0;
        }); // close popup win

        $("#wgl-win-close").off("click").on("click", this.close); // draw triangle

        d3.select("body").insert("div").attr("id", "triangle");
        var svg = d3.select('#triangle').append('svg').attr({
            'width': 35,
            'height': 35
        });
        var arc = d3.svg.symbol().type('triangle-down').size(function (d) {
            return 600;
        });
        svg.append('g').attr('transform', 'translate(' + 18 + ',' + 15 + ')').append('path').attr('d', arc).attr('fill', "#dce2e0");
    };

    this.close = function () {
        setVisibility(false);
        last_content = null;

        if ($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    };

    this.reloadContent = function () {
        if (enabled && visible && last_content) {
            replaceTable(prop2html(last_content));
        }
    };
    /**
     * Must be call after every zoom or move event
     * @param {int} zoom
     * @param offset
     */


    this.zoommove = function () {
        if (!visible) return;

        if (lngLat != null) {
            var point = translateCoordinatesToPoint(lngLat);
            setPosition(point.x, point.y);
        }
    };

    this.loadFilters = function () {
        var activeFilters = getUrlParameter(encodeURIComponent(idt_dim));

        if (activeFilters !== "") {
            var position = activeFilters.split(",");
            lngLat = {
                lng: parseFloat(position[0]),
                lat: parseFloat(position[1])
            };
            overlapping_points_index = parseInt(position[2]);
            showDialog(null, lngLat);
        }
    };

    var showDialog = function showDialog(e, preloadedLngLat) {
        if (typeof e === "undefined") {
            e = null;
        }

        if (typeof preloadedLngLat === "undefined") {
            preloadedLngLat = null;
        }

        if (_typeof(_this2.options) === "object" && _this2.options.hasOwnProperty('start')) {
            delete _this2.options['start'];
        }

        var clickPoint;

        if (geohash != null) {
            if (preloadedLngLat != null) {
                lngLat = preloadedLngLat;
                clickPoint = translateCoordinatesToPoint(lngLat);
            } else {
                clickPoint = {
                    x: e.offsetX,
                    y: e.offsetY
                };
                lngLat = translatePointToCoordinates(clickPoint);
            }

            geohash.load(lngLat.lng, lngLat.lat, geohash_level, geohash_path, function (data) {
                data = data.filter(function (a) {
                    return a['date'] !== "";
                });

                if (data.length === 1) {
                    var t = data[0];
                    overlapping_points_index = 0;
                    setVisibility(false);
                    last_content = t;
                    $("#wgl-point-win-head").css("border-top", "3px solid #ffa91b");
                    addContent(prop2html(t));
                    setVisibility(true);
                    setCenter(lngLat);
                    var center = getCenter();
                    var centerPoint = translateCoordinatesToPoint(center);
                    setPosition(centerPoint.x, centerPoint.y);
                } else {
                    if (overlapping_points_index > data.length) {
                        overlapping_points_index = 0;
                    }

                    data.sort(function (a, b) {
                        var dateA = new Date(a['date']);
                        var dateB = new Date(b['date']);

                        if (dateA < dateB) {
                            return 1;
                        }

                        if (dateA > dateB) {
                            return -1;
                        }

                        return 0;
                    });
                    setVisibility(false);
                    var currentSelectorWidth = dateSelectorWidth;

                    if (data.length <= 5) {
                        currentSelectorWidth = Math.floor(100 / data.length) + "%";
                    }

                    var selector = "<div style='" + "margin-left: -20px; " + "margin-right: -20px; " + "background-color: #122330;" + "margin-top: -10px;" + "padding-top: 5px;'>" + "<div style='display: inline-block; width: 7%;'><i id='date-selector-arrow-left' title='Previous date' style='cursor: " + (overlapping_points_index === 0 ? 'default' : 'pointer') + "; font-size: 25pt; color: " + (overlapping_points_index === 0 ? '#d9d9d9ff' : '#ffa91b') + "; font-weight: bold; opacity: 0.3' class='material-icons date-selector-arrow " + (overlapping_points_index === 0 ? "date-selector-arrow-inactive" : "") + "'>keyboard_arrow_left</i></div>" + "<div id='date-selector-container' style='display: inline-block; width: 86%; overflow: hidden;'>";
                    var parsedDate;
                    var formattedDate;

                    for (var i = 0; i < data.length; i++) {
                        if(hideDates) {
                            formattedDate = "Event "+(i+1);
                        } else {
                            parsedDate = new Date(data[i]['date']);
                            formattedDate = ("0" + parsedDate.getDate()).slice(-2) + "." + ("0" + (parsedDate.getMonth() + 1)).slice(-2) + "." + ("0" + parsedDate.getFullYear()).slice(-2);
                        }
                        selector += "<div class='date-selector " + (i === overlapping_points_index ? "date-active" : "") + "' data-index='" + i + "' style=\"height: 1.5em; padding-top: 2px; margin-bottom: 0.5em; padding-left: 3px; padding-right: 3px; min-width: " + dateSelectorWidth + "; cursor: pointer; border-top: 2px solid " + (i === overlapping_points_index ? "#ffa91b" : "transparent") + "; width:" + currentSelectorWidth + "; display: inline-block; text-align: center; color: white;\">" + formattedDate + "</div>";
                    }

                    selector += "</div><div style='display: inline-block; width: 7%;'><i id='date-selector-arrow-right' title='Next date' style='cursor: " + (overlapping_points_index === data.length - 1 ? 'default' : 'pointer') + "; font-size: 25pt; color: " + (overlapping_points_index === data.length - 1 ? '#d9d9d9ff' : '#ffa91b') + "; font-weight: bold; opacity: 0.3' class='material-icons date-selector-arrow " + (overlapping_points_index === data.length - 1 ? "date-selector-arrow-inactive" : "") + "'>keyboard_arrow_right</i></div>" + "</div>";
                    addContent(selector + prop2html(data[overlapping_points_index]));
                    $("#wgl-point-win-head").css("border-top", "none");
                    $(".date-selector-arrow").hover(function () {
                        if (!$(this).hasClass("date-selector-arrow-inactive")) {
                            $(this).fadeTo(400, 1);
                        }
                    }, function () {
                        if (!$(this).hasClass("date-selector-arrow-inactive")) {
                            $(this).fadeTo(400, 0.3);
                        }
                    });
                    var dateSelectorContainer = $("#date-selector-container");

                    if (overlapping_points_index !== 0) {
                        dateSelectorContainer.scrollLeft($(".date-active").position().left);
                    }

                    $("#date-selector-arrow-left").click(function () {
                        var index = parseInt($(".date-active").attr("data-index"));
                        var newIndex = index === 0 ? index : index - 1;

                        if (newIndex !== index) {
                            $(".date-selector[data-index=" + newIndex + "]").click();
                            dateSelectorContainer.scrollLeft($(".date-active").position().left);
                            overlapping_points_index = newIndex;
                        }

                        last_content = data[overlapping_points_index];

                        if (newIndex === 0) {
                            $("#date-selector-arrow-left").css("color", "#d9d9d9ff").css("cursor", "default").css("opacity", "0.3").addClass("date-selector-arrow-inactive");
                        } else {
                            $("#date-selector-arrow-left").css("color", "#ffa91b").css("cursor", "pointer").removeClass("date-selector-arrow-inactive");
                        }

                        if (newIndex === data.length - 1) {
                            $("#date-selector-arrow-right").css("color", "#d9d9d9ff").css("cursor", "default").css("opacity", "0.3").addClass("date-selector-arrow-inactive");
                        } else {
                            $("#date-selector-arrow-right").css("color", "#ffa91b").css("cursor", "pointer").removeClass("date-selector-arrow-inactive");
                        }
                    });
                    $("#date-selector-arrow-right").click(function () {
                        var index = parseInt($(".date-active").attr("data-index"));
                        var newIndex = index === data.length - 1 ? index : index + 1;

                        if (newIndex !== index) {
                            $(".date-selector[data-index=" + newIndex + "]").click();
                            dateSelectorContainer.scrollLeft($(".date-active").position().left);
                            overlapping_points_index = newIndex;
                        }

                        last_content = data[overlapping_points_index];

                        if (newIndex === data.length - 1) {
                            $("#date-selector-arrow-right").css("color", "#d9d9d9ff").css("cursor", "default").css("opacity", "0.3").addClass("date-selector-arrow-inactive");
                        } else {
                            $("#date-selector-arrow-right").css("color", "#ffa91b").css("cursor", "pointer").removeClass("date-selector-arrow-inactive");
                        }

                        if (newIndex === 0) {
                            $("#date-selector-arrow-left").css("color", "#d9d9d9ff").css("cursor", "default").css("opacity", "0.3").addClass("date-selector-arrow-inactive");
                        } else {
                            $("#date-selector-arrow-left").css("color", "#ffa91b").css("cursor", "pointer").removeClass("date-selector-arrow-inactive");
                        }
                    });
                    $(".date-selector").hover(function () {
                        if (!$(this).hasClass("date-active")) {
                            $(this).css("border-top", "2px solid #d9d9d9ff");
                        }
                    }, function () {
                        if (!$(this).hasClass("date-active")) {
                            $(this).css("border-top", "2px solid transparent");
                        }
                    });
                    $(".date-selector").click(function () {
                        $(".date-selector").css("border-top", "2px solid transparent").removeClass("date-active");
                        $(this).css("border-top", "2px solid #ffa91b");
                        $(this).addClass("date-active");
                        overlapping_points_index = parseInt($(this).attr("data-index"));
                        last_content = data[overlapping_points_index];
                        document.querySelector("#wgl-point-win-context table").outerHTML = prop2html(data[overlapping_points_index]);
                    });
                    setVisibility(true);
                    last_content = data[overlapping_points_index];
                    setCenter(lngLat);

                    var _center = getCenter();

                    var _centerPoint = translateCoordinatesToPoint(_center);

                    setPosition(_centerPoint.x, _centerPoint.y);
                }
            }, function () {
                WGL.getDimension(idt_dim).getProperties(clickPoint.x, clickPoint.y, function (t) {
                    setVisibility(false);
                    last_content = t;
                    $("#wgl-point-win-head").css("border-top", "3px solid #ffa91b");
                    addContent(prop2html(t));
                    overlapping_points_index = 0;
                    setVisibility(true);
                    setCenter(lngLat);
                    var center = getCenter();
                    var centerPoint = translateCoordinatesToPoint(center);
                    setPosition(centerPoint.x, centerPoint.y);
                }, _this2.options);
            });
        } else {
            if (preloadedLngLat != null) {
                lngLat = preloadedLngLat;
                clickPoint = translateCoordinatesToPoint(lngLat);
                overlapping_points_index = 0;
                WGL.getDimension(idt_dim).getProperties(clickPoint.x, clickPoint.y, function (t) {
                    setVisibility(false);
                    last_content = t;
                    addContent(prop2html(t));
                    setVisibility(true);
                    setCenter(lngLat);
                    var point = translateCoordinatesToPoint(lngLat);
                    setPosition(point.x, point.y);
                }, _this2.options);
            } else {
                WGL.getDimension(idt_dim).getProperties(e.offsetX, e.offsetY, function (t) {
                    if (pts != null) {
                        //get real coord of pt
                        var fMercZero = [];
                        var fMerc = [];
                        fMercZero[0] = pts[t.ID * 2];
                        fMercZero[1] = pts[t.ID * 2 + 1];
                        fMerc[0] = fMercZero[0] * (20037508.34 * 2 / 256) - 20037508.34;
                        fMerc[1] = -((fMercZero[1] - 256) * (20037508.34 * 2 / 256) + 20037508.34);
                        lngLat = new OpenLayers.LonLat(fMerc);
                    } else {
                        lngLat = translatePointToCoordinates({
                            x: e.offsetX,
                            y: e.offsetY
                        });
                    }

                    overlapping_points_index = 0;
                    setVisibility(false);
                    last_content = t;
                    addContent(prop2html(t));
                    setVisibility(true);
                    setCenter(lngLat);
                    var point = translateCoordinatesToPoint(lngLat);
                    setPosition(point.x, point.y);
                }, _this2.options);
            }
        }
    };
    /**
     * Private function for translating a pixel point to map coordinates
     * Compatible with both OpenLayers and Mapbox maps
     * Special thanks to user @ehanoj contribution on Github for his code and idea
     **/


    var translatePointToCoordinates = function translatePointToCoordinates(point) {
        var coordinates;

        if (typeof map.unproject === "function") {
            //mapbox lib
            coordinates = map.unproject(point);
        } else {
            // openlayers native*/
            coordinates = map.getLonLatFromPixel(point);
        }

        return coordinates;
    };
    /**
     * Private function for translating map coordinates to a pixel point in the map
     * Compatible with both OpenLayers and Mapbox maps
     * Special thanks to user @ehanoj contribution on Github for his code and idea
     **/


    var translateCoordinatesToPoint = function translateCoordinatesToPoint(coordinates) {
        var point;
        var np;

        if (typeof map.project === "function") {
            //mapbox lib
            point = map.project(coordinates);
        } else {
            // openlayers native*/
            point = map.getPixelFromLonLat(coordinates);
        }

        return point;
    };

    var getCenter = function getCenter() {
        if (typeof map.flyTo !== "undefined") {
            return map.getCenter();
        } else {
            return map.getView().getCenter();
        }
    };

    var setCenter = function setCenter(location) {
        if (typeof map.flyTo !== "undefined") {
            map.flyTo({
                center: [location.lng, location.lat],
                speed: 0.5,
                curve: 1,
                easing: function easing(t) {
                    return t;
                }
            }); //mapbox api
        } else {
            map.setCenter([location.lon, location.lat]); //openlayers api
        }
    };

    this.setup();
    WGL.ui.PopupWin.instance = this;
};