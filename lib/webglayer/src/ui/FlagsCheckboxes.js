"use strict";

WGL.ui.FlagsCheckboxes = function (m, div_id, filterId, flags_list, flags_names, params) {
    var _this = this;

    var that = this;
    var h;
    var permalink_input;
    var message;
    var exclusiveFlags;
    var threeColumnsLayout;
    var hideTotals;
    var smartCounts;
    var shouldZoomOnChartsWhenSelected;

    if (typeof params === "undefined") {
        h = "150px";
        permalink_input = null;
        message = null;
        exclusiveFlags = null;
        threeColumnsLayout = false;
        hideTotals = false;
        smartCounts = false;
        shouldZoomOnChartsWhenSelected = false;
    } else {
        h = params.h ? params.h : "150px";
        permalink_input = params.permalink_input ? params.permalink_input : null;
        message = params.message ? params.message : null;
        exclusiveFlags = params.exclusiveFlags ? params.exclusiveFlags : null;
        threeColumnsLayout = params.threeColumnsLayout ? params.threeColumnsLayout : false;
        hideTotals = params.hideTotals ? params.hideTotals : false;
        smartCounts = params.smartCounts ? params.smartCounts : false;
        shouldZoomOnChartsWhenSelected = params.shouldZoomOnChartsWhenSelected ? params.shouldZoomOnChartsWhenSelected : false;
    }

    this.getDivId = function () {
        return div_id;
    };

    this.init = function () {
        var loadedFlags = getUrlParameter(encodeURIComponent(m.name));

        if (loadedFlags !== "") {
            loadedFlags = loadedFlags.split(",");
            loadedFlags = loadedFlags.map(function (a) {
                return parseInt(a);
            });
        } else {
            loadedFlags = [];
        }

        var new_div = $("<div class='checkboxes-parent' style='height: " + h + "; font-size: 12px'></div>");
        $("#" + div_id).append(new_div);
        var inner_div = $("<div style='padding-top: 20px; margin: 0 auto; width: 95%;'></div>");
        new_div.append(inner_div);
        var new_checkbox;

        if (threeColumnsLayout) {
            var float = 'left',
                count_to_three = 0;

            for (var i = 0; i < flags_names.length; i++) {
                var count_selected = void 0,
                    count_unselected = void 0;

                if (!hideTotals) {
                    count_selected = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[i]]['selected']);
                    count_unselected = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[i]]['selected'] + WGL._dimensions[m.name].getCounts()[flags_list[i]]['unselected']);
                }

                if (loadedFlags.indexOf(i) !== -1) {
                    new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 33%; float: " + float + ";' ><span data-flag='" + i + "' id='flag_" + i + "' class='left' ><i style='font-size: 20px; color: #ffa91b' class='material-icons'>check_box</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + i + "'>" + flags_names[i] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count_selected flag_" + i + "' style='color: #ff8c00; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + count_selected + "</span> / <span class='count_unselected flag_" + i + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + count_unselected + "</span>)</span></span></div>");
                } else {
                    new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 33%; float: " + float + ";' ><span data-flag='" + i + "' id='flag_" + i + "' class='left' ><i style='font-size: 20px' class='material-icons'>check_box_outline_blank</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + i + "'>" + flags_names[i] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count_selected flag_" + i + "' style='color: #ff8c00; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> " + count_selected + "</span> / <span class='count_unselected flag_" + i + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + count_unselected + "</span>)</span></span></div>");
                }

                inner_div.append(new_checkbox);
                count_to_three++;

                if (count_to_three === 3) {
                    count_to_three = 0;
                    float = float === 'left' ? 'right' : 'left';
                }
            }
        } else if (smartCounts) {
            for (var _i = 0; _i < flags_names.length; _i++) {
                var _count_unselected = void 0;

                if (!hideTotals) {
                    _count_unselected = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[_i]]['selected'] + WGL._dimensions[m.name].getCounts()[flags_list[_i]]['unselected']);
                }

                if (_i % 2 === 0) {
                    if (loadedFlags.indexOf(_i) !== -1) {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: left;' ><span data-flag='" + _i + "' id='flag_" + _i + "' class='left' ><i style='font-size: 20px; color: #ffa91b' class='material-icons'>check_box</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i + "'>" + flags_names[_i] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count flag_" + _i + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected + "</span>)</span></span></div>");
                    } else {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: left;' ><span data-flag='" + _i + "' id='flag_" + _i + "' class='left' ><i style='font-size: 20px' class='material-icons'>check_box_outline_blank</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i + "'>" + flags_names[_i] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count flag_" + _i + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected + "</span>)</span></span></div>");
                    }

                    inner_div.append(new_checkbox);
                } else {
                    if (loadedFlags.indexOf(_i) !== -1) {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: right;' ><span data-flag='" + _i + "' id='flag_" + _i + "' class='right' ><i style='font-size: 20px; color: #ffa91b' class='material-icons'>check_box</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i + "'>" + flags_names[_i] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count flag_" + _i + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected + "</span>)</span></div>");
                    } else {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: right;' ><span data-flag='" + _i + "' id='flag_" + _i + "' class='right' ><i style='font-size: 20px' class='material-icons'>check_box_outline_blank</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i + "'>" + flags_names[_i] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count flag_" + _i + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected + "</span>)</span></div>");
                    }

                    inner_div.append(new_checkbox);
                }
            }
        } else {
            for (var _i2 = 0; _i2 < flags_names.length; _i2++) {
                var _count_selected = void 0,
                    _count_unselected2 = void 0;

                if (!hideTotals) {
                    _count_selected = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[_i2]]['selected']);
                    _count_unselected2 = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[_i2]]['selected'] + WGL._dimensions[m.name].getCounts()[flags_list[_i2]]['unselected']);
                }

                if (_i2 % 2 === 0) {
                    if (loadedFlags.indexOf(_i2) !== -1) {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: left;' ><span data-flag='" + _i2 + "' id='flag_" + _i2 + "' class='left' ><i style='font-size: 20px; color: #ffa91b' class='material-icons'>check_box</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i2 + "'>" + flags_names[_i2] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count_selected flag_" + _i2 + "' style='color: #ff8c00; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_selected + "</span> / <span class='count_unselected flag_" + _i2 + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected2 + "</span>)</span></span></div>");
                    } else {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: left;' ><span data-flag='" + _i2 + "' id='flag_" + _i2 + "' class='left' ><i style='font-size: 20px' class='material-icons'>check_box_outline_blank</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i2 + "'>" + flags_names[_i2] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count_selected flag_" + _i2 + "' style='color: #ff8c00; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> " + _count_selected + "</span> / <span class='count_unselected flag_" + _i2 + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected2 + "</span>)</span></span></div>");
                    }

                    inner_div.append(new_checkbox);
                } else {
                    if (loadedFlags.indexOf(_i2) !== -1) {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: right;' ><span data-flag='" + _i2 + "' id='flag_" + _i2 + "' class='right' ><i style='font-size: 20px; color: #ffa91b' class='material-icons'>check_box</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i2 + "'>" + flags_names[_i2] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count_selected flag_" + _i2 + "' style='color: #ff8c00; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> " + _count_selected + "</span> / <span class='count_unselected flag_" + _i2 + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected2 + "</span>)</span></div>");
                    } else {
                        new_checkbox = $("<div class='flag_checkbox' style='cursor: pointer; width: 48%; float: right;' ><span data-flag='" + _i2 + "' id='flag_" + _i2 + "' class='right' ><i style='font-size: 20px' class='material-icons'>check_box_outline_blank</i></span><label style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top' for='flag_" + _i2 + "'>" + flags_names[_i2] + "</label><span class='flag_checkbox_total' style='line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> (<span class='count_selected flag_" + _i2 + "' style='color: #ff8c00; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'> " + _count_selected + "</span> / <span class='count_unselected flag_" + _i2 + "' style='color: #8cc5f9; line-height: 20px; font-size: 13px; cursor: pointer; vertical-align: top'>" + _count_unselected2 + "</span>)</span></div>");
                    }

                    inner_div.append(new_checkbox);
                }
            }
        }

        if (message) {
            $("#" + div_id).append("<div class='message' style='padding: 10px 15px 15px 15px;'>" + message + "</div>");
        }

        $("#" + div_id + " .flag_checkbox").off("click").on("click", checkboxOnClick);
        var filter_clean = $("#chd-container-" + div_id + " .chart-filters-clean");
        filter_clean.off("click");
        filter_clean.on("click", function (e) {
            e.stopPropagation();
            that.clearSelection();
        });
        WGL.filterDim(m.name, filterId, loadedFlags);
        updateFiltersHeader(loadedFlags.length);

        if (permalink_input != null) {
            $("#" + div_id).on("chart:update-permalink", function (e) {
                e.stopPropagation();
                var oldURL = $("#" + permalink_input).val();

                if (WGL._dimensions[m.name].filters[filterId].isActive) {
                    var newURL = updateURLParameter(oldURL, encodeURIComponent(m.name), WGL._dimensions[m.name].filters[filterId].selected_flags);

                    if (oldURL !== newURL) {
                        $("#" + permalink_input).val(newURL);
                    }
                } else if (window.location.href.indexOf(m.name) !== -1) {
                    var _newURL = updateURLParameter(oldURL, encodeURIComponent(m.name), "");

                    if (oldURL !== _newURL) {
                        $("#" + permalink_input).val(_newURL);
                    }
                }
            });
        }

        if (hideTotals) {
            $(".flag_checkbox_total").hide();
        }
    };

    this.update = function () {
        if ($("#" + div_id + " .checkboxes-parent").length === 0) {
            _this.init();
        }
    };

    // Here numbers are updated when filters change
    this.updateTotals = function () {
        if (hideTotals) {
            return;
        }

        var count_selected;
        var count_unselected;
        var icon;
        var flag_element;

        if (smartCounts) {
            for (var i = 0; i < flags_list.length; i++) {
                var count = void 0;
                var color = void 0;
                flag_element = $("#" + div_id + " span.count.flag_" + i);

                if ($(flag_element.closest(".flag_checkbox").find("i")).text() === "check_box") {
                    count = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[i]]['selected']);
                    color = '#ff8c00';

                    flag_element.text(count);
                    flag_element.css("color", color);
                    flag_element.closest(".flag_checkbox_total").show();
                } else {
                    flag_element.closest(".flag_checkbox_total").hide();
                }


            }
        } else {
            for (var _i3 = 0; _i3 < flags_list.length; _i3++) {
                count_selected = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[_i3]]['selected']);
                count_unselected = numberWithSpaces(WGL._dimensions[m.name].getCounts()[flags_list[_i3]]['selected'] + WGL._dimensions[m.name].getCounts()[flags_list[_i3]]['unselected']);
                flag_element = $("#" + div_id + " span.count_selected.flag_" + _i3);
                flag_element.text(count_selected);
                flag_element = $("#" + div_id + " span.count_unselected.flag_" + _i3);
                flag_element.text(count_unselected);

                if (count_unselected === 0) {
                    flag_element.closest(".flag_checkbox").off("click");
                    flag_element.closest(".flag_checkbox").css("color", "lightgray").css("cursor", "default").find("i").css("color", "lightgray");
                } else {
                    flag_element.closest(".flag_checkbox").off("click");
                    flag_element.closest(".flag_checkbox").on("click", checkboxOnClick);
                    icon = flag_element.closest(".flag_checkbox").css("color", "black").css("cursor", "pointer").find("i");
                }
            }
        }
    };

    this.clean = function () {
        this.clearSelection();
    };

    this.clearSelection = function () {
        var icons = $("#" + div_id + " .flag_checkbox i");

        for (var i = 0; i < icons.length; i++) {
            if ($(icons[i]).text() === "check_box") {
                $(icons[i]).text("check_box_outline_blank").css("color", "black");
            }
        }

        WGL.filterDim(m.name, filterId, []);
        updateFiltersHeader(0);
        this.updateTotals();

        if (shouldZoomOnChartsWhenSelected) {
            automaticZoomCharts();
        }
    };

    var checkboxOnClick = function checkboxOnClick() {
        var this_index = parseInt($(this).find("[id^=flag]").data("flag"));

        if (exclusiveFlags && exclusiveFlags.indexOf(this_index) >= 0) {
            var filter_active = [];

            if ($(this).find("i").text() === "check_box") {
                $(this).find("i").text("check_box_outline_blank").css("color", "black");
            } else {
                var flags = $(this).parent().find("i");

                for (var i = 0; i < flags.length; i++) {
                    if ($(flags[i]).text() === "check_box") {
                        $(flags[i]).text("check_box_outline_blank").css("color", "black");
                    }
                }

                $(this).find("i").text("check_box").css("color", "#ffa91b");
                filter_active.push(this_index);
            }

            WGL.filterDim(m.name, filterId, filter_active);
            updateFiltersHeader(filter_active.length);
        } else {
            if ($(this).find("i").text() === "check_box") {
                $(this).find("i").text("check_box_outline_blank").css("color", "black");
            } else {
                $(this).find("i").text("check_box").css("color", "#ffa91b");
            }

            var _filter_active = [];

            var _flags = $(this).parent().find("i");

            for (var _i4 = 0; _i4 < _flags.length; _i4++) {
                if (exclusiveFlags && exclusiveFlags.indexOf(_i4) >= 0) {
                    $(_flags[_i4]).text("check_box_outline_blank").css("color", "black");
                } else if ($(_flags[_i4]).text() === "check_box") {
                    _filter_active.push(parseInt($(_flags[_i4]).parent().data("flag")));
                }
            }

            WGL.filterDim(m.name, filterId, _filter_active);
            updateFiltersHeader(_filter_active.length);
        }

        WGL.filterByExt();

        if (typeof updateTagsTotals !== "undefined") {
            updateTagsTotals();
        }

        if (shouldZoomOnChartsWhenSelected) {
            automaticZoomCharts();
        }
    };

    var updateFiltersHeader = function updateFiltersHeader(selected) {
        $("#chd-container-" + div_id + " .chart-filters-selected").html(selected);

        if (selected > 0) {
            $("#chd-container-footer-" + div_id + " .chart-filters-selected").html(selected);
            $("#chd-container-footer-" + div_id).removeClass("hide");
        } else {
            $("#chd-container-footer-" + div_id).addClass("hide");
        }

        if ($(".active-filters-container [id^=chd-container-footer]:not(.hide)").length > 0) {
            $(".close-item-filters").removeClass("hide");
            $("#active-filters-placeholder").addClass("hide");
            $(".active-filters-item .bar-item").addClass("bar-item-active");
            $(".active-filters-container").slideDown();
        } else {
            $("#active-filters-placeholder").removeClass("hide");
            $(".close-item-filters").removeClass("hide");
        }

        if ($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }

        if (typeof modifyCanvasCor !== "undefined") {
            modifyCanvasCor();
        }
    };

    var automaticZoomCharts = function automaticZoomCharts() {
        if ($(".flag_checkbox i:not(:contains(check_box_outline_blank))").length > 0) {
            $(".select-legend-scale").attr("data-previously-selected", "true");

            if (!$(".legend-selected").hasClass("select-legend-scale")) {
                $(".legend-selected i").click();
            }
        } else {
            $(".legend-selected i").click();
            var chart_contents = $(".chart-content");

            for (var i = 0; i < chart_contents.length; i++) {
                if ($("#" + $($(".chart-content")[i]).attr("id") + " [data-previously-selected=true]").length > 0 && !$("#" + $($(".chart-content")[i]).attr("id") + " [data-previously-selected=true]").hasClass("legend-selected")) {
                    $("#" + $($(".chart-content")[i]).attr("id") + " [data-previously-selected=true] i").click();
                } else {
                    $("#" + $($(".chart-content")[i]).attr("id") + " .legend-out i").click().click();
                }
            }

            $(".select-legend-scale").removeAttr("data-previously-selected");
        }
    };
};