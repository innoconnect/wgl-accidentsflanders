"use strict";

let map;
let draw;
let mapColor = 'dark-v9';
let polygons = [];
let control_options;
$(document).ready(function () {
    let zoom = getUrlParameter("zoom");
    let center = getUrlParameter("center");

    mapboxgl.accessToken = 'pk.eyJ1IjoiamlyaS1iIiwiYSI6ImNqZmNjajc1MTJjN2cyeG5ycG5lcWhpNHMifQ.d-4wK9BUDPUHq_SRgHYe9g';
    map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/dark-v9?optimize=true', // stylesheet location 'mapbox://styles/mapbox/dark-v9'
        center: (center === "" ? [4.342310121240189, 51.0740164241798] : center.split(",")), // starting position [lng, lat]
        zoom: (zoom === "" ? 8.190883244797387 : zoom), // starting zoom
        preserveDrawingBuffer: true
    });

    // disable map rotation using right click + drag
    map.dragRotate.disable();

    // disable map rotation using touch rotation gesture
    map.touchZoomRotate.disableRotation();

    draw = new MapboxDraw({
        displayControlsDefault: false,
        controls: {
            polygon: true,
            trash: true
        }
    });
    map.addControl(draw, 'top-left');

    const nav = new mapboxgl.NavigationControl();
    map.addControl(nav, 'bottom-right');

    control_options = {"draw": draw.modes.DRAW_POLYGON, "delete": draw.modes.SIMPLE_SELECT};
});

function transform(x, y) {
    const tl = getTopLeftTC();
    const p = new mapboxgl.LngLat(y, x);
    const v = map.project(p);
    return toLevel0(v, tl, map.getZoom());
}
function toLevel0(pt, tl, zoom) {
    const scale = Math.pow(2, zoom);
    pt.x = pt.x / scale + tl.x;
    pt.y = pt.y / scale + tl.y;
    return pt;
}
function toggleColorScheme(element) {
    const schemes = $(element).parent().parent().find(".color-scheme-btn");
    for (let i=0; i<schemes.length; i++) {
        if (schemes[i] === element) {
            schemes[i].classList.add("layer-selected");
        } else {
            schemes[i].classList.remove("layer-selected");
        }
    }

    const scheme = $(element).data("scheme");
    WGL.colorSchemes.setSchemeSelected(scheme);
    changeMapColor();

    if(getCookie("acceptCookies") === "true") {
        setCookie("heatmap-color", scheme);
    }
}

function changeMapColor() {
    const newMapColor = WGL.colorSchemes.getSchemeBgSelected();
    if(newMapColor !== mapColor) {
        map.setStyle('mapbox://styles/mapbox/' + newMapColor);
        mapColor = newMapColor;
    }

    if(typeof onMove !== "undefined") {
        setTimeout(onMove, 300);
    }
}
