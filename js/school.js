"use strict";

const SchoolsLayer = function(map_win_id) {

    if(typeof SchoolsLayer.instance === "object") {
        return SchoolsLayer.instance;
    }

    this.dictionary = ["School details", "Name", "Address", "Category", "Nursery school", "Primary school", "Secondary school", "Basic education"];

    this.lastEvent = null;

    this.popup = null;

    let enabled = true;

    let permalink_input = null;

    this.setEnabled = function(b) {
        enabled = b;
    };

    this.getEnabled = function() {
      return enabled;
    };

    this.addSchool = (callback) => {

        if(typeof map.getSource("points") !== "undefined") {
            return;
        }

        const filename = "data/school.geojson";

        const that = this;

        d3.json(filename, (data) => {
            map.loadImage("data/school_light_small.png", function (error, image) {
                map.addImage('wgl-school', image, {"sdf": "true"});
                map.addLayer({
                    "id": "points",
                    "type": "symbol",
                    "source": {
                        "type": "geojson",
                        "data": data
                    },
                    "layout": {
                        "icon-image": "wgl-school",
                        "icon-size": 0.12,
                        "visibility": "none"
                        //"icon-allow-overlap": true
                    },
                    "paint": {
                        "icon-color": { "type": "identity", "property": "color" }
                    }
                });

                map.off('click', 'points', that.showSchool);
                map.on('click', 'points', that.showSchool);

                // Change the cursor to a pointer when the mouse is over the places layer.
                map.on('mouseenter', 'points', function () {
                    map.getCanvas().style.cursor = 'pointer';
                });

                map.on('mouseover', 'points', function () {
                    map.getCanvas().style.cursor = 'pointer';
                });

                // Change it back to a pointer when it leaves.
                map.on('mouseleave', 'points', function () {
                    map.getCanvas().style.cursor = '';
                });

                callback();
            });

        });
    };

    this.showSchool = (e) => {

        if(enabled) {

            this.lastEvent = e;

            const coordinates = e.features[0].geometry.coordinates.slice();
            const properties = e.features[0].properties;

            let p = properties;

            let translatedCategorie;

            if (p.Categorie === "Kleuteronderwijs") translatedCategorie = this.getDictionary()[4];
            if (p.Categorie === "Lager onderwijs") translatedCategorie = this.getDictionary()[5];
            if (p.Categorie === "Secundair onderwijs") translatedCategorie = this.getDictionary()[6];
            if (p.Categorie === "Basiseducatie") translatedCategorie = this.getDictionary()[7];
            if (p.Categorie === "Hoger onderwijs") translatedCategorie = this.getDictionary()[8];

            let aa =
                `<div class="wgl-schools-selection-win">
                          <div id="wgl-schools-win-head">${this.getDictionary()[0]}</div><div id="wgl-schools-win-close" ><i class="fa fa-times" aria-hidden="true" onclick="SchoolsLayer.closeDialog()"></i></div>
                          <div id="wgl-schools-win-context">
                          <table>
                              <tbody>
                                  <tr><td>${this.getDictionary()[1]}:</td><td>${p.POI_Naam}</td></tr>
                                  <tr><td>${this.getDictionary()[2]}:</td><td>${p.POI_Straat} ${p.POI_Huisnummer}, ${p.POI_Postcode} ${p.POI_Gemeente}</td></tr>
                                  <tr><td>${this.getDictionary()[3]}:</td><td>${translatedCategorie}</td></tr>
                               </tbody>
                          </table>
                          </div>
                      </div>`;

            map.flyTo({
                center: e.features[0].geometry.coordinates,
                speed: 0.4,
                curve: 1,
                easing(t) {
                    return t;
                }
            });

            map.once('moveend', () => {
                $("#school-dialog").html(aa).css("bottom", map.project(coordinates).y + 35).css("left", map.project(coordinates).x-40).show();
                $("#triangle-school").css("bottom", map.project(coordinates).y).css("left", map.project(coordinates).x-17).show();
            });
        }
    };

    this.configurePermalinkInput = function(p) {
        permalink_input = p;

        if(permalink_input != null) {

            if(!map_win_id) {
                map_win_id = ".mapboxgl-canvas";
            }

            $(map_win_id).off("schools:update-permalink").on("schools:update-permalink", () => {

                let oldURL = $("#" + permalink_input).val();

                if (typeof $(".wgl-schools-selection-win").css("display") !== "undefined" && this.lastEvent) {

                    oldURL = updateURLParameter(oldURL, "sc", "");
                    oldURL = updateURLParameter(oldURL, "sp", "");

                    const sc = this.lastEvent.features[0].geometry.coordinates.slice();
                    const sp = this.lastEvent.features[0].properties;

                    let newURL = updateURLParameter(oldURL, 'sc', sc.toString());
                    newURL = updateURLParameter(newURL, 'sp', encodeURIComponent(JSON.stringify(sp)));
                    if (oldURL !== newURL) {
                        $("#" + permalink_input).val(newURL);
                    }
                } else {
                    let newURL = updateURLParameter(oldURL, 'sc', "");
                    newURL = updateURLParameter(newURL, 'sp', "");
                    if (oldURL !== newURL) {
                        $("#" + permalink_input).val(newURL);
                    }
                }
            });
        }
    };

    this.loadFilters = () => {
        let sc = getUrlParameter('sc');
        let sp = decodeURIComponent(getUrlParameter('sp'));
        if (sc !== "" && sp !== "") {
            let e = {features: [{"geometry" : {"coordinates": sc.split(',')}, "properties": JSON.parse(sp)}]};
            this.showSchool(e);
        }
    };

    this.reloadSchool = () => {
      if(this.lastEvent
        && $("#school-dialog").css("display") !== "none") {
          SchoolsLayer.closeDialog();
          this.showSchool(this.lastEvent);
      }
    };

    SchoolsLayer.closeDialog = () => {
        $("#school-dialog").html("").hide();
        $("#triangle-school").hide();
    };

    this.setDictionary = (d) => {
        if(d instanceof Array) {
            this.dictionary = d;
        }
    };

    this.getDictionary = () => {
        return this.dictionary;
    };

    SchoolsLayer.instance = this;
};