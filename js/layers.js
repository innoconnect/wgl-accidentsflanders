"use strict";

const moveCanvasLayer = () => {
    if (typeof map !== "undefined" && typeof map.getLayer("canvas") !== "undefined") {
        let roadLargeId;
        const layers = map.getStyle().layers;
        for (let i=0; i<layers.length; i++) {
            if (layers[i].id === "road-label-large") {
                roadLargeId = layers[i+1].id;
                map.moveLayer("canvas", roadLargeId);
            }
        }
    } else {
        setTimeout(moveCanvasLayer, 500);
    }
};

moveCanvasLayer();