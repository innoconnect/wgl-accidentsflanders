const BoundaryLayer = function () {
  this.add = (callback) => {

    const filename = "data/road_bound10.geojson";
    const filename_road = "data/vvrr.geojson";

    if(map.getLayer("boundary") !== undefined || map.getLayer("boundary-name") !== undefined) {
      return;
    }

    d3.json(filename, (data) => {
      map.addLayer({
        "id": "boundary",
        "type": "line",
        "source": {
          "type": "geojson",
          "data": data
        },
        "layout": {
          //"visibility": "none"
          "line-cap": "round",
          "line-join": "round",
          "visibility": "none"
        },
        "paint": {
          "line-color": "#00aedb",
          "line-width": 3,
          "line-opacity": 1,
          "line-dasharray": [0, 2]

        }
      });
    });

    d3.json("data/road_name10.geojson", (data) => {

      try {

        map.addLayer({
          "id": "boundary-name",
          "type": "symbol",
          "source": {
            "type": "geojson",
            "data": data
          },
          "layout": {
            "text-field": "{naam}",
            "text-size": 12,
            "visibility": "none"
          },
          "paint": {
            "text-color": "#00aedb"
          }
        });
      } catch (e) {
          console.log(e);
      }
    });

    // === ROAD BOUNDARY ===
    d3.json(filename_road, (data) => {
      map.addLayer({
        "id": "boundary-road",
        "type": "line",
        "source": {
          "type": "geojson",
          "data": data
        },
        "layout": {
          "visibility": "none"
          //"line-cap": "round",
          //"line-join": "round",
          //"visibility": "none"
        },
        "paint": {
          "line-color": "#CA4699",
          "line-width": 4,
          "line-opacity": 1,
          "line-dasharray": [2, 0.8]

        }
      });
    });

    d3.json("data/vvrr_name.geojson", (data) => {

      try {

        map.addLayer({
          "id": "boundary-name-road",
          "type": "symbol",
          "source": {
            "type": "geojson",
            "data": data
          },
          "layout": {
            "visibility": "none",
            "text-field": "{VVRR}",
            "text-size": 12
            //"visibility": "none"
          },
          "paint": {
            "text-color": "#CA4699"
          }
        });
      } catch (e) {
        console.log(e);
      }
    });

      d3.json("data/average_speed_4326.geojson", (data) => {

          try {

              map.addLayer({
                  "id": "average-speed",
                  "type": "line",
                  "source": {
                      "type": "geojson",
                      "data": data
                  },
                  "layout": {
                    "visibility": "none"
                  },
                  "paint": {
                      "line-color": "#ff8000",
                      "line-width": 4,
                      "line-opacity": 1,
                      //"line-dasharray": [2, 0.8]

                  }
              });
          } catch (e) {
              console.log(e);
          }
      });

  };
};